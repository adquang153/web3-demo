import React, { createContext, useContext, useState } from 'react';

export const InitWeb3Context = createContext();

export default function Web3Context() {
    return useContext(InitWeb3Context);
}

export const Web3ContextProvider = ({ children }) => {

    const [web3, setWeb3] = useState();

    const getChainId = async () => {
        try{
            if (!web3) return;
            let newChainId = await web3.eth.getChainId()
            newChainId = web3.utils.toHex(newChainId);
            return newChainId;
        }catch(error){
            console.error(error);
            return null;
        }
    }

    const getAccounts = async () => {
        try {
            const accounts = await web3.eth.getAccounts();
            return accounts;
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    const sendTransaction = async () => {
        try {
          // Get user's Ethereum public address
          const fromAddress = (await web3.eth.getAccounts())[0];
    
          const destination = fromAddress;
    
          const amount = web3.utils.toWei("0.001"); // Convert 1 ether to wei
    
          // Submit transaction to the blockchain and wait for it to be mined
          const receipt = await web3.eth.sendTransaction({
            from: fromAddress,
            to: destination,
            value: amount,
            maxPriorityFeePerGas: "5000000000", // Max priority fee per gas
            maxFeePerGas: "6000000000000", // Max fee per gas
          });
    
          return receipt;
        } catch (error) {
          return error;
        }
      }
    
      const signMessage = async () => {
        try {
          // Get user's Ethereum public address
          const fromAddress = (await web3.eth.getAccounts())[0];
    
          const originalMessage = "YOUR_MESSAGE";
    
          // Sign the message
          const signedMessage = await web3.eth.personal.sign(
            originalMessage,
            fromAddress,
            "test password!" // configure your own password here.
          );
    
          return signedMessage;
        } catch (error) {
          return error;
        }
      }

    const getBalance = async()=>{
        try {
            const accounts = await getAccounts();
            const balance = await web3.eth.getBalance(accounts[0]);
            return balance;
        } catch (error) {
            return error;
        }
    }

    return <InitWeb3Context.Provider value={{ getChainId, getAccounts, signMessage, getBalance, sendTransaction, setWeb3 }}>
        {children}
    </InitWeb3Context.Provider>
}
