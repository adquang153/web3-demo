import { useEffect, useState } from "react";
import { Web3Auth } from "@web3auth/web3auth";
import { CHAIN_NAMESPACES } from "@web3auth/base";
import Web3Context from "./context/Web3Context";
import { SPECTRE } from "./configs/chains";
import "./App.css";
import Web3 from "web3";

const clientId =
  "BP0V6gFAhLQ6RL3MG7zQ59YoUyA9hKWa_hi7Qn9DjT3cqRY4WuuQP7WThHArzlKZqihHb4NppBGStFBJZ5gCcNI"; // get from https://dashboard.web3auth.io

function App() {
  const {
    sendTransaction,
    signMessage,
    getBalance,
    getChainId,
    getAccounts,
    setWeb3,
  } = Web3Context();

  const [web3auth, setWeb3auth] = useState();
  const [provider, setProvider] = useState();
  const [data, setData] = useState();

  useEffect(() => {
    const init = async () => {
      try {
        const web3auth = new Web3Auth({
          clientId,
          chainConfig: {
            chainNamespace: CHAIN_NAMESPACES.EIP155,
            chainId: SPECTRE.chainId,
            rpcTarget: SPECTRE.rpcUrls[0], // This is the public RPC we have added, please pass on your own endpoint while creating an app
          },
          uiConfig: {
            theme: "dark",
          },
        });

        setWeb3auth(web3auth);

        await web3auth.initModal();
        if (web3auth.provider) {
          setProvider(web3auth.provider);
          setWeb3(new Web3(web3auth.provider));
        }
      } catch (error) {
        console.error(error);
      }
    };

    init();
  }, []);

  const login = async () => {
    if (!web3auth) {
      alert("web3auth not initialized yet");
      return;
    }
    const web3authProvider = await web3auth.connect();
    setProvider(web3authProvider);
    setWeb3(new Web3(web3auth.provider));
  };

  const getUserInfo = async () => {
    if (!web3auth) {
      alert("web3auth not initialized yet");
      return;
    }
    const user = await web3auth.getUserInfo();
    setData({ user });
  };

  const logout = async () => {
    if (!web3auth) {
      alert("web3auth not initialized yet");
      return;
    }
    await web3auth.logout();
    setProvider(null);
  };

  const getMyChainId = async () => {
    const chainId = await getChainId();
    setData({ chainId });
  };
  const _getAccounts = async () => {
    const address = await getAccounts();
    setData({ address });
  };

  const getMyBalance = async () => {
    const balance = await getBalance();
    setData({ balance });
  };

  const _sendTransaction = async () => {
    setData("sending transaction...");
    const receipt = await sendTransaction();
    setData({ receipt });
  };

  const _signMessage = async () => {
    const signedMessage = await signMessage();
    setData({ signedMessage });
  };

  const _getPrivateKey = async () => {
    try {
      const privateKey = await provider.request({
        method: "eth_private_key",
      });

      setData({ privateKey });
    } catch (error) {
      return error;
    }
  };

  const loggedInView = (
    <>
      <button onClick={getUserInfo} className="card">
        Get User Info
      </button>
      <button onClick={getMyChainId} className="card">
        Get Chain ID
      </button>
      <button onClick={_getAccounts} className="card">
        Get Accounts
      </button>
      <button onClick={getMyBalance} className="card">
        Get Balance
      </button>
      <button onClick={_sendTransaction} className="card">
        Send Transaction
      </button>
      <button onClick={_signMessage} className="card">
        Sign Message
      </button>
      <button onClick={_getPrivateKey} className="card">
        Get Private Key
      </button>
      <button onClick={logout} className="card">
        Log Out
      </button>

      <div
        style={{
          border: "1px solid #333",
          borderRadius: "4px",
          padding: "16px",
          width: "100%",
          marginTop: "20px",
          wordBreak: "break-all",
          overflow: "scroll"
        }}
      >
        <pre>{JSON.stringify(data, null, "\t")}</pre>
      </div>

      <div id="console" style={{ whiteSpace: "pre-line" }}>
        <p style={{ whiteSpace: "pre-line" }}></p>
      </div>
    </>
  );

  const unloggedInView = (
    <button onClick={login} className="card">
      Login
    </button>
  );

  return (
    <div className="container">
      <h1 className="title">
        <a target="_blank" href="http://web3auth.io/" rel="noreferrer">
          Web3Auth
        </a>{" "}
        Demo
      </h1>

      <div className="grid">{provider ? loggedInView : unloggedInView}</div>
    </div>
  );
}

export default App;
